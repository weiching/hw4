/*
 * @author weiching
 */
package hw4;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class client extends JFrame {

	private JPanel contentPane;
	private JTextField txtLocalhost;
	private JTextField textField_port;
	private JTextField textField_myport;
	private JLabel lblTrackerid;
	private JLabel lblPort;
	private JTable table;
	private JTable table_1;
	private JList list;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					client frame = new client();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public client() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 720, 594);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtLocalhost = new JTextField();
		txtLocalhost.setText("localhost");
		txtLocalhost.setBounds(86, 10, 117, 21);
		contentPane.add(txtLocalhost);
		txtLocalhost.setColumns(10);
		
		textField_port = new JTextField();
		textField_port.setText("5000");
		textField_port.setColumns(10);
		textField_port.setBounds(253, 10, 107, 21);
		contentPane.add(textField_port);
		
		textField_myport = new JTextField();
		textField_myport.setText("8888");
		textField_myport.setColumns(10);
		textField_myport.setBounds(477, 42, 107, 21);
		contentPane.add(textField_myport);
		
		lblTrackerid = new JLabel("TrackerID:");
		lblTrackerid.setBounds(10, 13, 76, 15);
		contentPane.add(lblTrackerid);
		
		lblPort = new JLabel("port:");
		lblPort.setBounds(213, 13, 30, 15);
		contentPane.add(lblPort);
		
		JButton btnConnect = new JButton("Connect");
		btnConnect.setBounds(370, 9, 87, 23);
		contentPane.add(btnConnect);
		
		JButton btnDisconnect = new JButton("Disconnect");
		btnDisconnect.setBounds(467, 9, 117, 23);
		contentPane.add(btnDisconnect);
		
		JButton btnAddDirectory = new JButton("Add directory");
		btnAddDirectory.setBounds(10, 41, 124, 23);
		contentPane.add(btnAddDirectory);
		
		JButton btnupload = new JButton("upload my list");
		btnupload.setBounds(144, 41, 124, 23);
		contentPane.add(btnupload);
		
		JButton btnclean = new JButton("clean my list");
		btnclean.setBounds(278, 41, 124, 23);
		contentPane.add(btnclean);
		
		JLabel lblMyList = new JLabel("My port:");
		lblMyList.setBounds(426, 49, 76, 15);
		contentPane.add(lblMyList);
		
		JList list = new JList();
		list.setBounds(10, 74, 323, 472);
		contentPane.add(list);
		
		table_1 = new JTable();
		table_1.setBounds(343, 74, 351, 472);
		contentPane.add(table_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(305, 74, 28, 472);
		contentPane.add(scrollPane);
		
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(664, 74, 30, 472);
		contentPane.add(scrollPane_1);
		
		this.setTitle("HW4");
		
		
		
		/*=============================================================================*/
	
	}
}
